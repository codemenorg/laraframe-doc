# Directory Structure
```
Visual Editor
│
└─── element-library
│   |
│   └─── element.js
│
│
└─── images
│   |
│   └─── * image needs to be placed here .....
│
│
└─── output
│   |
│   └─── * saved files are stored here .....
│
│
└─── vendor
│   |
│   └─── * all custom plugins .....
│   
│   
└─── visual-editor
│   |
│   └─── class-settings.js
│   |
│   └─── input-type.js
│   |
│   └─── style-inputs.js
│   |
│   └─── visual-editor.js
│   |
│   └─── visual-editor-events.js
│   |
│   └─── visual-editor-functions.js
│   |
│   └─── visual-editor-general-settings.js
│   |
│   └─── visual-editor-initialization.js
│   |
│   └─── visual-editor-primary-elements.js
│
│
└─── bootstrap.bundle.min.js
│
└─── bootstrap.min.css
│
└─── bootstrap.min.js
│
└─── cmb-style.css
│
└─── demo-audio.mp3
│
└─── demo-video.mp4
│
└─── dropdown.png
│
└─── dynamic-element.php
│
└─── index.html
│
└─── jquery-3.4.1.min.js
│
└─── live-page.js
│
└─── no-valid-image.jpg
│
└─── parser.php
│
└─── popper.min.js
│
└─── save-me.php
│
└─── slider.js
│
└─── slider-builder.js
│
└─── style.css
│
└─── texture.jpg
│
└─── visual-editor-element-style.css
│
└─── visual-editor-style.css
```






















