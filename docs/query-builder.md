## Introduction

Laraframe Query Builder gives you some exteded functionalities over Laravel Query Builder

## List of Query Builder Functions

** All builtin laravel query builder functions **

#### bulkUpdate():

Update multiple rows at a times by different conditions for each row.

`parameters: attributes:array` 

`return: updated row count` 

Example:

``` 
$parameters = [
    [
        'conditions' => ['is_active' => 1,
        'is_email_verified' => 1],
        'fields' => [
            'first_name' => 'John',
            'last_name' => 'Doe'
        ]
    ],
    [
        'conditions' => ['email' => 'robin@example.com', 
        'is_active' => 1],
        'fields' => [
            'is_financial_suspended' => 1,
        ],
    ],
];
$userUpdateCount = User::bulkUpdate($parameters);
```

    

Fields key can update different ways. Example Below:

``` 
'fields' => [
    'column_name1' => ['increment', 10],
    'column_name2 => ['decrement', 5],
    'column_name3' => ['raw', 'column_name1 - column_name2'],
    'column_name4' => 'value',
];
```

#### toggleStatus():

Change status of a row by its id. By default it changes `is_active` column. If you want to change different column then pass the column name as parameter.

`parameters: attribute(optional)` 

`return: boolean` 

Example:

``` 
$user = User::find(1);
$isChanged = $user->toggleStatus(1, 'is_financial_suspended');
```

    

#### active():

This function is a global query scope. It will get all active data. By default it use `is_active` column. If you want to change different column then pass the column name as parameter.

#### likeSearch():

This function is a global query scope. You can search with this functions. It is usages mysql wildcard search.
`parameters: array $searchFields,  string $term` 
This function is a global query scope. It will get all active data. By default it use `is_active` column. If you want to change different column then pass the column name as parameter.

#### strictSearch():

This function is a global query scope. You can search with this functions. It is usages strict search conditions.
`parameters: array $searchFields,  string $term` 

