used Class: app/Services/Core/FileUploadService

function: 
```
upload($file, $filePath, $fileName, $prefix = '', $suffix = '', $disk = null, $width = null, $height = null, $fullView=true, $fileExtension = 'png', $quality = 100)


--------------------

file: file input

filePath: path where file will be saved

prefix: prefix will be added at the beginning of the filename.

suffix: suffix will be added at the end of the filename

disk: file storage disk. see laravel storage system.

width: width of the saved image. if only width is given, image will be resized keeping the width aspect ratio.

heigh: height of the saved image. if only height is given, image will be resized keeping the height aspect ratio.

fullView: if $fullview is true and width and height are given, the image will be resized in the given area; otherwise it will be resized and cropped.

fileExtension: the target extension of saved file

quality: image quality. value is integer between 1 to 100.



Note: if width and height both are null, image will be saved in full
```