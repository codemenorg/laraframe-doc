The language managements where you can create, update and delete languages. And from the language settings you can add, modify or delete any translation. Also you can sync all new translations from your project.  


#### Settings

In the language setting page you can choose any language from the dropdown. From the translations list you can select any for modify and delete that translation. Modify translation will just update the selected language translation but delete action will delete that translation from all languages. If you delete a translation but that stay in your project will come back when you press `Sync` button. And the `Sync` button will also added new translations from your project. 

By press `Add New` button you can crate a new translation and this translation will added to all others language also. 


![Language Settings Page](./images/language-setting.png)


#### Application Settings for Language

* **Default Site Language:** By this option you can select default language for your site. 
* **Language Switcher:** By this option you can enable/disable for language switcher. 
* **Display Language Switch Item:** By this option you can select which item will display in the language switcher. 







