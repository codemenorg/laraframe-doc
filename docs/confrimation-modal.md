There are 3 types of confirmation modal

1. Link Confirmation
2. Auto form submit confirmation especially for put/delete and sometime post methods. it is used to submit a form with an id as input
3. Existing form submit confirmation

**Handler Class:** `confirmation`

Link Confirmation Example `<a class="confirmation" data-alert="your warning message" href="http://yourLinkUrl.com">`

Auto form confirmation example: `<a class="confirmation" href="http://yourFormActionUrl.com" data-form-id="anythingRandomText" data-alert="your warning message" data-form-method="put">`

Existing form confirmation example: `<a class="confirmation" href="javascript:;" data-form-id="yourFormId" data-alert="your warning message">`

######Options
|Option|Required|Uses|
|------|--------|----|
|href|required for link/auto form|This is the link where will be redirected to. For auto form it is the form action url|
|data-alert|required|This is the message appears as warning on clicking the link|
|data-form-id|Only use for form|For auto form you can place any random text. For existing form, it must be the form ID|
|data-form-method|Only use for Auto form|To confirm which method will be used to submit the form|