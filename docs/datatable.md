By using `DataTableService` you can build a list page just in a minutes.

``` 
$searchFields = [
        ['username', __('Username')],
        ['email', __('Email')],
        ['first_name', __('First Name')],
        ['last_name', __('Last Name')],
    ];
    $orderFields = [
        ['first_name', __('First Name')],
        ['last_name', __('Last Name')],
        ['email', __('Email')],
        ['username', __('Username')],
        ['users.created_at', __('Registered Date')],
    ];

    $select = [
        'users.*', 'name', 'first_name', 'last_name'
    ];

    $filterFields = [
        ['users.role_id', __('Role'), Role::pluck('name', 'id')->toArray()],
        ['users.is_active', __('Status'), account_status()],
    ];

    $queryBuilder = User::select($select)
        ->leftJoin('user_profiles', 'users.id', '=', 'user_profiles.user_id')
        ->leftJoin('roles', 'users.role_id', '=', 'roles.id')
        ->orderBy('users.id', 'desc');

    $downloadableHeadings = [
        'users.id' => __('ID'),
        'users.username' => __('Username'),
        'user_profiles.first_name' => __('First Name'),
        'user_profiles.last_name' => __('Last Name'),
        'users.email' => __('Email'),
        'users.is_email_verified' => __('Email Verified Status'),
        'users.is_financial_active' => __('Financial Status'),
        'users.is_active' => __('Status')
    ];

    $data['dataTable'] = app(DataTableService::class)
        ->setPaginationInfo(['pageName' => 'p', ['itemPerPage' => 15]])
        ->setSearchFields($searchFields)
        ->setOrderFields($orderFields)
        ->setFilterFields($filterFields)
        ->downloadable($downloadableHeadings)
        ->create($queryBuilder);
```

In the above code `$searchFields` is the data that will be visible in search field dropdown in view. The data will be an array and the array item will also be an array. The array item will have 2 items inside it. First item is the actual field name and last item is showable field name in view.

``` 

    $searchFields = [
        ['fieldname', __('Viewable name')],
    ];

```

In the above code `$orderFields` is the data that will be visible in order field dropdown in view. Rule is same as $searchFields

``` 
                                                                                                                               
    $orderFields = [
        ['fieldname', __('Viewable name')],
    ];
        
```

$select is optional and if used it is an array of field names.

``` 
                                                                                                $select = ['table1.field1', 'table1.field2', 'table2.field3'];
        
```

$filters is optional and is only used if filter is needed on data list view. This is an array of items and every item is also array. In the item array, There are 3 types of filtering.

``` 
    $filters = [
        ['field1', 'showable label 1', $array1],
        ['field2', showable label 2, 'range'],
        ['field3', showable label 3, 'preset', 
            [
                ['individual label 1', $operator1, $value1],
                ['individual label 2', $operator2, $value2],
            ]
        ],
    ];
```

First item type

``` 
    *** This works as or condition. It means any of the selected option will be displayed*** 
    *** First field name, next: label and last array*** 
    'field1', 'showable label 1', $array1],
```

Range item type

``` 
    *** This works with minimum and maximum input field***  
    *** First field name, next: label and last one is fixed value - 'range'*** 
    ['field2', showable label 2, 'range'],
```

preset item type

``` 
    *** This works with minimum and maximum input field***  
    *** First field name, next: label, next: fixed value - 'preset' and last one is multi dimentional array *** 

    *** Multi Dimensional Array: it contains array as items. The items must have individual option label first, next: $operator and last $value ***
    
    *** Supported operators are given below ***
    ***
        '=', '<', '>', '<=', '>=', '<>', '!=', '<=>',
        'like', 'like binary', 'not like', 'ilike',
        '&', '|', '^', '<<', '>>', 'rlike', 'regexp',
        'not regexp', '~', '~*', '!~', '!~*', 'similar to',
        'not similar to', 'not ilike', '~~*', '!~~*',
    ***

    *** Example ***
    ['field3', showable label 3, 'preset', 
        [
            ['individual label 1', $operator1, $value1],
            ['individual label 2', $operator2, $value2],
        ]
    ],
```

In the above code `$downloadableHeadings` is the heading and cloumns for CSV and PDF.

``` 
                                                                                                                               
    $downloadableHeadings = [
        ['fieldname', __('Heading name')],
    ];
        
```



#### Available Functions

|name|Parameter| Required | Description |
|:------|:-------:|:-------:|:-----------|
|setSearchFields|array|No|Searchable fields name and Labels|
|setOrderFields|array|No|Orderable fields name and Labels|
|setPaginationInfo|array|No|Pagination information|
|setFilterFields|array|No|Filterable filelds and Labels|
|downloadable|array|No|Downlaodable filelds and Headings|
|create|Object|Yes|Query builder object|


After calling `create` function in `DataTableService` will return an array.
The array cointains `filters`, `items`, `paginations` keys. 
When you set filters it will give you addtional key `advanceFilters`,
All the keys except `items` cointains HTML view. `items` cointains collections. 


###### Use in Blade 

``` 
@extends('layouts.master')
@section('title', $title)
@section('content')
    <div class="container my-5">
        <div class="row">
            <div class="col-lg-12">
                {{ $dataTable['filters'] }}
                {{ $dataTable['advanceFilters'] }}
                <div class="my-4">
                @component('components.table',['class'=> 'lf-data-table'])
                    @slot('thead')
                        <tr class="bg-primary text-white">
                            <th class="all">{{ __('Email') }}</th>
                            <th class="min-phone-l">{{ __('First Name') }}</th>
                            <th class="min-phone-l">{{ __('Last Name') }}</th>
                            <th class="min-phone-l">{{ __('User Group') }}</th>
                            <th class="min-phone-l">{{ __('Username') }}</th>
                            <th class="none">{{ __('Registered Date') }}</th>
                            <th class="text-center min-phone-l">{{ __('Status') }}</th>
                            <th class="text-right all no-sort">{{ __('Action') }}</th>
                        </tr>
                    @endslot

                    @foreach($dataTable['items'] as $key=>$user)
                        <tr>
                            <td>
                                @if(has_permission('users.show'))
                                    <a href="{{ route('users.show', $user->id) }}">{{ $user->email }}</a>
                                @else
                                    {{ $user->email }}
                                @endif
                            </td>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->name}}</td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->created_at->format('Y-m-d') }}</td>
                            <td class="text-center">{{ view_html($user->is_active ? '<i class="fa fa-check text-success"></i>' : '<i class="fa fa-times text-danger"></i>') }}</td>
                            <td class="lf-action text-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-info dropdown-toggle"
                                            data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-gear"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                                        <a class="dropdown-item" href="{{ route('users.show',$user->id)}}"><i
                                                class="fa fa-eye"></i> {{ __('Show') }}</a>
                                        <a class="dropdown-item" href="{{ route('users.edit',$user->id)}}"><i
                                                class="fa fa-pencil-square-o fa-lg text-info"></i> {{ __('Edit Info') }}
                                        </a>
                                        <a class="dropdown-item" href="{{ route('users.edit.status',$user->id)}}"><i
                                                class="fa fa-pencil-square fa-lg text-info"></i> {{ __('Edit Status') }}
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endcomponent
                </div>
                {{ $dataTable['pagination'] }}
            </div>
        </div>
    </div>
@endsection

@section('style')
    @include('layouts.includes.list-css')
@endsection

@section('script')
    @include('layouts.includes.list-js')
@endsection
```

Total view example is given above.

`{{ $dataTable['filters'] }}` will load filter section

`{{ $dataTable['advanceFilters'] }}` will load advance filter section

`{{ $dataTable['pagination'] }}` will load pagination section


Classes for datatable

|Class| Required | Belongs to | Description |
|:---:|:--------:|:----------:|:-----------:|
|lf-data-table|Yes|table , component|it loads datatable|
|all|no|th|it always keeps the column visible|
|min-phone-l|no|th|it auto hides the column on responsive if not enough space|
|none|no|th|It always keeps the column hide|
|no-sort|no|th|It disables sorting for the column|
|lf-action|no|td|Required on td if dropdown menu is taken under the td|

The script below must be called

``` 
@section('style')
    @include('layouts.includes.list-css')
@endsection

@section('script')
    @include('layouts.includes.list-js')
@endsection
```

