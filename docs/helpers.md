## Helpers

In `Helpers` files there are set of constant and function that you can use anywhere in the project even in the view. All helpers files are located in `app\Helpers` directory. If you want to create another helper function then create a php file inside `app\Helpers` directory and add the file path into `composer.json` file > `files` section bellow our helper files.

    "autoload": {
            "files": [
                "app/Helpers/helpers.php",
                "app/Helpers/corearrays.php",
                "app/Helpers/coreconstants.php"
                "app/Helpers/new.php"
            ]
        }
    

`corearrays.php` holds all of your list options. For example active status. You call the function with a key for the key value.

    function active_status($input = null) {
            $output = [
                ACTIVE_STATUS_ACTIVE => __('Active'),
                ACTIVE_STATUS_INACTIVE => __('Inactive'),
            ];
    
            return is_null($input) ? $output : $output[$input];
        }
    

`coreconstants.php` holds all of your application specific constants.

`helpers.php` holds all of your application specific functions.

List of helper functions.

**company_name():** return: company name.

**company_logo():** return: company logo.

**random_string():** parameters: length, characters (optional) return: string

**settings():** parameters: key, fromDatabase:boolean (optional) return: value

**check_language():** parameters: language return: language (if not exists return null)

**set_language():** parameters: language, default language (optional) return: nothing

**language():** return: list of languages.

**fake_field():** parameters: field_name return: fake_field_name or plain field_name if the field is not found in fake_field lists.

**base_key():** return: encrypted key

**encode_decode():** parameters: string, decode:boolean (optional) return: encoded string or decoded string if decode true

**validate_date():** parameters: date, separator(optional) return: boolean

**has_permission():** parameters: route_name, user_id(optional), is_link(optional), is_api(optional) return: boolean

**is_json():** parameters: string return: boolean

**is_current_route():** parameters: route_name, active_class_name(optional), must_have_route_parameters(optional), optional_route_parameters(optional) return: active_class_name or empty string if not found.
```
must_have_route_parameters: the parameters that must be available to make the target html tag active. must be array or null. If data exists, parameter value must match the given condition with the given value
optional_route_parameters: the parameters that may or may not be available to make the target html tag active. must be array or null. If data exists, parameter value must match the given condition with the given value or parameter must be empty to get active status.

------------------------
Array pattern of must_have_route_parameters and optional_route_parameters:
pattern 01: ['slug'=>'value', ....]
pattern 02: [['slug', '>','value'], .....]
pattern 03: [['slug', 'value'], ....]

combined or same pattern can be used. for patter 02, comparable parameters are >|>=|<|<=|=|!=
pattern 01 and 03 refers equal comarison
```

**available_in_parameters():** parameters: route_parameter, optional_parameters(optional) return: boolean

**return_get():** parameters: key, value(optional) return: ' selected' or empty string if not found.

**array_to_string():** parameters: array,separator(optional), key:boolean(optional), is_seperator_at_ends:boolean(optional) return: string

**valid_image():** parameters: path, image_name return: boolean

**get_avatar():** parameters: avatar_name return: absolute path of the avatar

**get_user_specific_notice():** parameters: user_id(optional) return: last 5 notice and unread notice count

**get_nav():** parameters: place, template(optional) return: html content of the menu

**get_breadcrumbs():** return: array of urls

**get_system_notices():** return: list of system notices

**get_available_timezones():** return: list of timezone

**form_validation():** parameters: errors, field_name return: bootstrap class

**get_user_roles():** return: list of user roles

**get_image():** parameters: image_name return: absolute path of the image