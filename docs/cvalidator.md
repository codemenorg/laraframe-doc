#### File Required to include:
```
cvalidator.js
cvalidator-language-en.js
```

**Language file can be recreated for individual language**

#### Call cValidator
**Call cvalidator with a ID**
```
let cvalidator = $('#idName').cValidate({})
```

#### Options

|Option|Type  |DefaultValue|Required|Description|
|------|------|------------|--------|-----------|
|rules |Object| N/A        | Yes    |key as input name. value is rules with string. Details in description|
|messages|Object| N/A      |Optional|key as input name + rule. value is Message with string. Details in description|
|commonFieldName|Object|N/A|Optional|key as input name. value is custom field name with string. details in description|
|customRules|Object|N/A    |Optional|new rule if needed from outside. new rule name as key and value is created callable function|
|formGroupClass|String|form-group|optional|html/css class name for input wrapper. change if required|
|messageClass|String|invalid-feedback|optional|html/css class name for error message. must be in formGroupClass|
|submitButtonClass|String|form-submission-button|optional|html/css class name for submit button.|
|messageOnSubmit|boolean|false|optional|If true message will be shown on modal instead of default form error message|
|formSubmission|boolean|true|optional|If false, form submit is disabled. required for custom ajax call|

##### Rules
key as input name. for array key, array item will be added as '.' for example: if field name is `name[firstname]` key will be `name.firstname`. wildcard key is possible. for example: if field names are `name[firstname]` and `name[lastname]` key will be `name.*`. value is rules with string. rules will be separated with '|' a rule may have have parameter and parameter value. parameter will be added with rule with ':' and parameter values will be added with , . `i.e- rule:parameter,value1,value2|rule|rule:parameter`
##### messages
key as input name + rule. `name.required`. for array key, array item will be added as '.' for example: if field name is `name[firstname]` key will be `name.firstname.required`. wildcard key is possible. for example: if field names are `name[firstname]` and `name[lastname]` key will be `name.*.required`.
##### commonFieldName
key as input name. if different name is required in error message. for array key, array item will be added as '.' for example: if field name is `name[firstname]` key will be `name.firstname`. wildcard key is possible. for example: if field names are `name[firstname]` and `name[lastname]` key will be `name.*`.

#### Callback Methods

##### reformat(inputArray)
`parameter : 1 | Optional | array of input field name`

if array is not passed all input will be reformatted. on passing array, only passed fields will be reformatted. `cvalidator.refromat(inputArray)`

##### refreshDom()
refresh the whole html inside form.  `cvalidator.refreshDom()`

##### getErrorMessage()
returns error message.  `cvalidator.getErrorMessage()`

##### resetRules(newRules)
replace the existing rules.  `cvalidator.resetRules(newRules)`

##### setRules(newRules)
replace only passed rules.  `cvalidator.setRules(newRules)`

##### getRules()
return all rules.  `cvalidator.setRules()`


#### Rule options
|          |          |        |          |        |
|:--------:|:--------:|:------:|:--------:|:------:|
|url|required|email|integer|numeric|
|max|min|between|in|notIn|
|files|alpha|alphaNum|alphaSpace|alphaDash|
|image|digitsOnly|strongPassword|follow|followedBy|
|time|dateTime|date|escapeInput|escapeText|
| - |-|escapeFullText|-| - |

######url 
checks if the field value is valid url

######required
makes the field must fillable

######email
checks if the field value is valid email

######integer
checks if the field value is integer

######numeric
checks if the field value is numeric

######max:value
The field under validation must be less than or equal to a maximum value. Strings, numerics, arrays, and files are evaluated in the same fashion as the 'size' note.

######min:value
The field under validation must have a minimum value. Strings, numerics, arrays, and files are evaluated in the same fashion as the 'size' note.

######between:min,max
The field under validation must have a size between the given min and max. Strings, numerics, arrays, and files are evaluated in the same fashion as the 'size' note.

```
Size Note:
string:if the field value is string minimum or maximum value means the number of characters,
integer:if the field value is string minimum or maximum value means the value of numbers,
numeric:if the field value is string minimum or maximum value means the numeric data,
files:if the field value is string minimum or maximum value means the value of file size,
```

######in:foo,bar,...
The field under validation must have one of the given values.

######notIn:foo,bar,...
The field under validation must not have any of the given values.

######files:
filetype1,filetype2,... - The field under validation must have one of the given filetypes.

######alpha
The field under validation must be alphabet

######alphaNum
The field under validation may only have alphabet and number or comibination

######alphaSpace
The field under validation may only have alphabet and space.

######alphaDash
The field under validation may only have alphabet, underscore and hypen.

######image
image file format. 

######digitsOnly
The field under validation may only have digits.

######strongPassword
The field under validation must contain at least 1 Uppercase, 1 lowercase, 1 number and 1 special characters

######same
The field under validation must have same value as the given field name

######time
The field under validation must be valid time. i.e 15:20:12 or 15:20

######dateTime
The field under validation must be valid date time. i.e 2019-10-15 15:20:12

######date
The field under validation must be valid date. i.e 2019-10-15

######escapeInput
The field under validation must not contain any tag

######escapeText
The field under validation must not contain any tag except `<p>,<br>,<b>,<i>,<u>,<strong>,<ul>,<ol>,<li>`

######escapeFullText
The field under validation must not contain any tag except `<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<hr>,<article>,<section>,<video>,<audio>,<table>,<tr>,<td>,<thead>,<tfoot>,<footer>,<header>,<p>,<br>,<b>,<i>,<u>,<strong>,<ul>,<ol>,<dl>,<dt>,<li>,<div>,<sub>,<sup>,<span>,<img>,<a>`


