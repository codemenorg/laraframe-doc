After successfully installing on production server you need to active your application. To active it you need 2 things: 

1. Purchase Code 
2. Domain Configured

**Note: Make sure you have configured your domain correctly. Without domain you cannot active your application.**

![Product Activation](./images/activation.png)


You need to active your exchange when:

1. First time installation
2. If you update all of your existing code. 
3. If you move your server to another server

**Note: Product activation won't necessary when you install it on your local pc for development and if you change/modified some files (not all) on production server.**



