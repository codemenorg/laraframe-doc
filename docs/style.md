#### Default styling: scss
Path: `resources/sass/`

All files are included in app.scss when different components are inside `resources/sass/components/`

`
Any new component can be added in currnt directories or inside a new directory if required. The new file must be included in `app.scss` mentioned above
`

##### Base SCSS
Path: `resources/sass/components/base/`

`_variables.scss` Contains all `scss variables` and `functions`

##### Core Components
Path: `resources/sass/components/core/`


`bootstrap_custom.scss` Contains all changed style of `bootstrap 4`

`breadcrumb.scss` Contains all style of `breadcrumb`

`custom_inputs.scss` Contains all style of `input type html`

`datatable.scss` Contains all style of `datatable`

`datetimepicker.scss` Contains all style of `date time picker`

`dropdown_language.scss` Contains all style of `language dropdown` in top header

`dropdown_notification.scss` Contains all style of `notification dropdown` in top header

`dropdown_user.scss` Contains all style of `user dropdown menu` in top header

`flash_message.scss` Contains all style of `flash message box` developed in laraframe

`footer.scss` Contains all style of `footer`

`list_filter.scss` Contains all style of `filter` section in a 'list' page

`list_pagination.scss` Contains all style of `pagination` section in a 'list' page

`list_search.scss` Contains all style of `search` section in a 'list' page

`menu-builder.scss` Contains all style of `visual menu builder` section in admin

`side_nav.scss` Contains all style of `left side navigation` section

`top_nav.scss` Contains all style of `top navigation` section

`primary.scss` Contains all style of all other styles that are not categorized.

###### Usable CSS Classes

|Class | Structure | Nested | Parent | Description |
|:----:|:---------:|:------:|:------:|:-----------:|
|centralize-wrapper|possible tag is div|Yes|-|It is used to take a section vertically and horizontally centered|
|centralize-inner|possible tag is div|Yes|centralize-wrapper|-|
|centralize-content|possible tag is div|Yes|centralize-inner|Content will go inside it|
|lf-checkbox|possible tag is div|Yes|-|Custom checkbox. input type checkbox and label will be inside the div and they will be siblings themselves|
|inactive|label|No|lf-checkbox|inactive checkbox style. There will be no input field in this case|
|active|label|No|lf-checkbox|active checkbox style. There will be no input field in this case|
|lf-radio|possible tag is div|Yes|-|Custom checkbox. input type radio and label will be inside the div and they will be siblings themselves|
|inactive|label|No|lf-radio|inactive checkbox style. There will be no input field in this case|
|active|label|No|lf-radio|active checkbox style. There will be no input field in this case|
|lf-switch|possible tag is div|Yes|-|Custom checkbox. input type radio and label will be inside the div and they will be siblings themselves|
|lf-switch-input|input type radio|No|lf-switch|-|
|lf-switch-label|label|No|lf-switch|-|
|lf-select|possible tag is div|Yes|-|Custom select style. Select tag will be inside it|
|line-height-standard|all tags|No|-|Standard line height. Value of line height is 1|
|line-height-medium|all tags|No|-|Medium line height. Value of line height is 1.5|
|line-height-maximum|all tags|No|-|Maximum line height. Value of line height is 2|
|img-table|possible tag is td|No|-|Set table cell image widht 30px|
|lf-w-*px|all tags|No|-|Custom width class. Replace * number between 10 and 200, and increment value is 10|
|lf-h-*px|all tags|No|-|Custom height class. Replace * number between 10 and 200, and increment value is 10|
|border-gray-11|all tags|No|-| Border light gray class|
|lf-flex-*|possible tag is div|No|flex|Custom flex ratio class. Replace * number between 1 and 10, and increment value is 1|
|lf-cursor-pointer|possible tags are a,li,button,span|No|-|Cursor pointer class|
|lf-pre-wrap|possible tag is div|No|-|Pre wrap class|
