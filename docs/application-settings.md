This section describes how you can manage your application settings in `config/appsettings.php`

## Application Settings

Application settings where you can control all of your settings like enable/disable any features or upload logo, change your application name etc. There is no need to go to application settings core code. You just need to add a setting inside `appsettings.php`. It will generate a form field automatically for you.

Example:
```
'settings' => [
    'preference' => [
        'icon' => 'fa-home',
        'sub-settings' => [
            'general' => [
                'company_name' => [
                    'field_type' => 'text',
                    'validation' => 'required',
                    'field_label' => 'Company Name',
                ],
            ],
        ],
    ],
    'layout' => [
        'icon' => 'fa-align-center',
        'sub-settings' => [
            'logo-and-icon' => [
                'company_logo' => [
                    'field_type' => 'image',
                    'height' => 120,
                    'width' => 120,
                    'validation' => 'image|size:512',
                    'field_label' => 'Company Logo',
                ],
            ],
        ],
    ],
],
```

**Note: Icon only allow for parent setting.**

For user friendly URL it's better to place `-` separator for multi words setting and sub setting name. For example `payment-methods`

#### Accessing Settings

All settings are store into `application_settings` table. Every time when you save the settings it will also store into cache. When you call `settings('field_name')` it will bring value from cache. If you need to access setting value directly from database then you can call `settings('field_name', true)`. 

You can also get multiple settings value at once by passing array of field names as bellow:

```
$settings = settings(['field_name1', 'field_name2', 'field_name3]);
```


#### Options:

|Option| Required | Values | Description |
|:--------:|:--------:|:------:|:-----------:|
| field_type |	Yes 	|text, textarea, select, checkbox, radio, switch, image| 	The field type that we support now.|
| field_label |	Yes | string | Define you field label.|
| field_value |	No | string, array | Function name or array of values. Applicable for field_types: select, checkbox, radio |
| default | No |	mixed |	Default value of the field. |
| display_when | No | associative array | Display when given field is equal to specific value. |
| placeholder |	No | string | Placeholder can only be used in text and textarea. |
| validation | No | integer, numeric, email, digit, boolean, required, size, min, max, array, in |	These are the validation rules and only size is applicable for field_type image. For `in` you can use any function that return array or you can also use comma separated value |
| previous | No | key of any previous setting | If any value is used before and you want to use it again without calling the data, just call the previous setting key name. |
| encryption |	No | boolean | If the value is true then the field value will be saved encrypted. |
| input_class |	No | string | Add custom class for your input field. |
| section_start_tag | No | string |	Change start tag for particular setting. |
| section_end_tag | No | string | Change end tag for particular setting. |
| slug_start_tag | No |	string | Change start tag for particular setting label. |
| slug_end_tag | No | string | Change end tag for particular setting label. |
| value_start_tag |	No | string | Change start tag for particular setting value. | 
| value_end_tag | No | string |	Change end tag for particular setting value. | 


All the settings below are customizable in the config file
```
    *** Wrapper for an application setting ***

    'common_wrapper' => [
        'section_start_tag' => '',
        'section_end_tag' => '',
        'slug_start_tag' => '',
        'slug_end_tag' => '',
        'value_start_tag' => '',
        'value_end_tag' => '',
    ],



    *** Wrapper for an input text ***

    'common_text_input_wrapper' => [
        'input_start_tag' => '',
        'input_end_tag' => '',
        'input_class' => '',
    ],



    *** Wrapper for a textarea ***

    'common_textarea_input_wrapper' => [
        'input_start_tag' => '',
        'input_end_tag' => '',
        'input_class' => '',
    ],



    *** Wrapper for a select ***

    'common_select_input_wrapper' => [
        'input_start_tag' => '',
        'input_end_tag' => '',
        'input_class' => '',
    ],



    *** Wrapper for an input checkbox ***

    'common_checkbox_input_wrapper' => [
        'input_start_tag' => '',
        'input_end_tag' => '',
        'input_class'=>'',
    ],



    *** Wrapper for an input radio ***

    'common_radio_input_wrapper' => [
        'input_start_tag' => '',
        'input_end_tag' => '',
        'input_class' => '',
    ],



    *** Wrapper for an input toggle ***

    'common_toggle_input_wrapper' => [
        'input_start_tag' => '',
        'input_end_tag' => '',
        'input_class'=>'',
    ],
```
