# **Welcome to Laraframe Documantation** 


![My logo](images/logo-full.png){: style="max-width:250px"}

## **Introduction**
Laraframe v4 is built with Laravel 7.x with required core functionalities.

## **Technology used**

* **Php 7.4+**
* **MySql 5.7+**
* **Laravel 7.x**

## **Server Requirements**

* **PHP >= 7.4.0**
* **BCMath PHP Extension**
* **Ctype PHP Extension**
* **JSON PHP Extension**
* **Mbstring PHP Extension**
* **OpenSSL PHP Extension**
* **PDO PHP Extension**
* **Tokenizer PHP Extension**
* **XML PHP Extension**
* **GD PHP Extension**

## **Features**
* **Visual Navigation manager**
* **Dynamic Role Management System**
* **Extended Artisan Commands**
* **Codeless Ajaxification**
* **Laravel alike jquery frontend validation**
* **User Management**
* **Profile Management**
* **System Notice Managment**
* **Ready-made User Notification**
* **Ready-made Maintenance mode**
* **Status Based auto restriction**
* **Ready-made Login, signup, email verification and reset password**
* **Google ReCaptcha**
* **Laravel Debagbar**
* **Log Viewer**


