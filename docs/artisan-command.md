Artisan is the command-line interface included with Laraframe. It provides a number of helpful commands that can assist you while you build your application. To view a list of all available Artisan commands, you may run `php artisan list` command.

## Additional Artisan Command List

**Clear Command:** Execute `php artisan clear:all` artisan command will clear `config`, `cache`, `route` and `view` cache. You can also clear `config` and `cache` only by running `php artisan clear:all --only=config,cache` or `php artisan clear:all --except=route,view`

**Make Service Command:** Execute `php artisan make:service` artisan command will create a new Service inside your `Services` directory.

**Make View Command:** Execute `php artisan make:view` artisan command will create a new view inside your `resources/views` directory.