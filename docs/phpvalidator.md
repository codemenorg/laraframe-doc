####Rules
|          |          |        |          |        |
|:--------:|:--------:|:------:|:--------:|:------:|
|hash_check|digits_only|date_gt|date_gte|date_lt|
|date_lte|date_neq|date_eq|slug|alpha_space|

######hash_check:hased_value
checks if the field value matched with the given hashed value

######digits_only
The field under validation must be 0-9 digits only

`For the next 6 validation rule, laravel built in validation rule "date" is required. Otherwise it may give error`

######date_gt:another_field,this_field_format,other_field_format
The field under validation checks if date of this field is greater than given field. `this_field_format` and
`other_field_format` are optional. by default format is `Y-m-d H:i:s`

######date_gte:another_field,this_field_format,other_field_format
The field under validation checks if date of this field is greater than or equal to given field. `this_field_format` and
`other_field_format` are optional. by default format is `Y-m-d H:i:s`

######date_lt:another_field,this_field_format,other_field_format
The field under validation checks if date of this field is less than given field. `this_field_format` and
`other_field_format` are optional. by default format is `Y-m-d H:i:s`

######date_lte:another_field,this_field_format,other_field_format
The field under validation checks if date of this field is less than or equal to given field. `this_field_format` and
`other_field_format` are optional. by default format is `Y-m-d H:i:s`

######date_neq:another_field,this_field_format,other_field_format
The field under validation checks if date of this field different from given field. `this_field_format` and
`other_field_format` are optional. by default format is `Y-m-d H:i:s`

######date_eq:another_field,this_field_format,other_field_format
The field under validation checks if date of this field is equal to given field. `this_field_format` and
`other_field_format` are optional. by default format is `Y-m-d H:i:s`

######slug
The field under validation must have alphabates and hypen.

######alpha_space
The field under validation must have alphabates and space.