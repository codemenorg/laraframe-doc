## Menu Manager

The Menu Manager where you can manage your menu. It is a visual menu manager. You can build and use it anywhere in your project by using helper function `get_nav($place)`. By default one place is registered that is `back-end`. If you want to register another place then add you place inside `config/navigation.php` file under `registered_place` section just like bellow:

```
'registered_place' => [
    'top-nav',
    'profile-nav',
    'side-nav',
],
```

You can chagne the menu templete or create another new template from `config/navigation`.php

```
'navigation_template' =>[
    'default_nav' => [
        *** Beginning wrapper of a menu ***
        'navigation_wrapper_start'=> '<ul>',
        
        *** Ending wrapper of a menu ***
        'navigation_wrapper_end'=> '</ul>',
        
        *** Beginning wrapper of a menu Item ***
        'navigation_item_wrapper_start'=> '<li>',
        
        *** Ending wrapper of a menu Item ***
        'navigation_item_wrapper_end'=> '</li>',

        
        *** Beginning wrapper inside a menu Item before text ***
        'navigation_item_beginning_wrapper_start'=> '',
        
        *** Ending wrapper inside a menu Item before text ***
        'navigation_item_beginning_wrapper_end'=> '',
        
        *** Beginning wrapper of a menu Item text ***
        'navigation_item_text_wrapper_start'=> '<p>'
        
        *** Ending wrapper of a menu Item text ***,
        'navigation_item_text_wrapper_end'=> '</p>',
        
        *** Beginning wrapper of a menu Item after text ***
        'navigation_item_ending_wrapper_start'=> '',
        
        *** Ending wrapper of a menu Item after text ***
        'navigation_item_ending_wrapper_end'=> '',
        
        *** Beginning wrapper of a menu Item icon ***
        'navigation_item_icon_wrapper_start'=> '<i class="nav-icon">',
        
        *** Ending wrapper of a menu Item icon ***
        'navigation_item_icon_wrapper_end'=> '</i>',

        *** Icon position ***
        'navigation_item_icon_position'=> 'text-left',      
        : top-left | top-right | bottom-left | bottom-right | text-left | text-right | full-right | full-left

        
        *** Beginning wrapper of a submenu ***
        'navigation_sub_menu_wrapper_start'=> '<ul class="nav nav-treeview">', 

        *** Ending wrapper of a submenu ***
        'navigation_sub_menu_wrapper_end'=> '</ul>',

        *** Beginning wrapper of a submenu item ***
        //'navigation_item_wrapper_in_sub_menu_start'=> '<li>',

        *** Ending wrapper of a submenu item ***
        //'navigation_item_wrapper_in_sub_menu_end'=> '</li>',


        *** Class for menu item ***
        'navigation_item_link_class'=> 'nav-link',

        *** Class for active menu item ***
        'navigation_item_link_active_class'=> 'active',

        *** Positon of active menu class in a tag ***
        'navigation_item_active_class_on_anchor_tag'=> true,   : true | false

        *** Navigation item default url if not available ***
        'navigation_item_no_link_text'=> '',                : # | javascript:;


        *** Beginning wrapper of Megamenu ***
        'mega_menu_wrapper_start'=>'<div class="megamenu-container">',

        *** Ending wrapper of Megamenu ***
        'mega_menu_wrapper_end'=>'</div>',

        *** Beginning wrapper of Megamenu section ***
        'mega_menu_section_wrapper_start'=>'<div class="megamenu-section">',

        *** Ending wrapper of Megamenu section ***
        'mega_menu_section_wrapper_end'=>'</div>'
        ],
    ],
```

**N:B: Following keys are required. Others are optional**

```
'navigation_wrapper_start'=> '',
'navigation_wrapper_end'=> '',
'navigation_item_wrapper_start'=> '',
'navigation_item_wrapper_end'=> '',
```

To create new menu template just copy the default_nav section and rename it. Change the tags, classes as you want. **Don't change the key name. It will break all of your menu.**
