## Initialization

Create a js file and add that file right after `<script src="visual-editor/visual-editor-primary-elements.js"></script>` and do not change the orders of other scripts at all.

Next follow the structure given below. 

```
cmbAllElements['cmb_your_custom_element']= {
    // Options goes here
    // methods goes here
};


```
Custom element key must have a prefix `cmb_` and will be snake case (Mandatory).


### Options

#### Available Options
|          |          |        |          |
|:--------:|:--------:|:------:|:--------:|
|allowedParent|title|addButtonText|elementTabName|
|html|htmlVariation|subWrapper|hideActionSymbol|
|customAction|allowedActionElement|avoidableTabs|avoidableTabSettings|
|avoidElementListing|tabSettingOptionOnly|avoidableTabSettingOptions|settings|
|elementType|icon|dynamicOptions||



##### allowedParent: 

**type:** (array) [] 

**required:** True

**value:** element name of targeted parent from cmbAllElements object keys or `content-wrapper`

**Note:** can be multiple. This is used to define under which element we can add this element.


##### title: 
**type:** string 

**required:** True

**value:** any custom name

**Note:** This name is used in action element bar (move delete setting option bar) and setting bar


##### elementType: 
**type:** string 

**required:** True

**value:** `dynamic-layout` or `static-layout`

**Note:** dynamic layout is for if data comes from database and static layout is if data is built without server script support


##### icon: 
**type:** string 

**required:** True

**value:** fontawesome icon can be used

**Note:** it is used in action element bar


##### addButtonText: 
**type:** string 

**required:** True

**value:** any custom name

**Note:** This name is used in add element tab when allowed parent add button is clicked.


##### elementTabName: 
**type:** string 

**required:** True

**value:** we have some built in tab name that can be used
```
    container
    component
    header
    media
    pre-build-template
    dynamic-layout
```
if custom tab name is required add the variable key once
```
cmbElementTabs['key'] = 'value'
```
and then use the key several times in tabname 

**Note:** This tabname is shown in element library tab


##### html: 
**type:** string 

**required:** True if `htmlVariation` is not added

**value:** html text of your element and this html grand wrapper must have the exact 


##### htmlVariation: 
**type:** (object) {} 

**required:** True if `html` is not added

**value:** 
```
htmlVariation : {
    'blank' : {
        subtitle: 'something',
        icon: '<i class="cm cm-section-blank"></i>',
        html: `<tag></tag>`
    },
    'filled' : {
        subtitle: 'something else',
        icon: '<i class="cm cm-section-blank"></i>',
        html: `<tag></tag>`
    },
}
```
`htmlVariation` and each variation as object where `subtitle` `icon` and `html` is required. html will follow the html rules.

##### hideActionSymbol: 
**type:** (object) {} 

**required:** True if `html` is not added

**value:** 
```
htmlVariation : {
    'blank' : {
        subtitle: 'something',
        icon: '<i class="cm cm-section-blank"></i>',
        html: `<tag></tag>`
    },
    'filled' : {
        subtitle: 'something else',
        icon: '<i class="cm cm-section-blank"></i>',
        html: `<tag></tag>`
    },
}
```
`htmlVariation` and each variation as object where `subtitle` `icon` and `html` is required. html will follow the html rules.

##### subWrapper: 
**type:** boolean 

**required:** optional

**value:** if true, action element bar (move delete setting option bar) will have no icon and name

##### customAction: 
**type:** (object) {} 

**required:** optional

**value:** 
```
customAction:{
    'add-row' : {title : 'add row',text : '<i class="fa fa-bars"></i>'},
    'add-col' : {title : 'add column',text : '<i class="fa fa-columns"></i>'},
},
```

if a custom button is required in action element bar (move delete setting option bar), it can be added this way and custom jquery can be called with `$('.cmb-custom-action[data-action="add-row"])`. // add-row will be replaced with your key here.

##### avoidElementListing: 
**type:** boolean 

**required:** optional

**value:** if true, item will not be appeared in element modal as adding option

##### allowedActionElement: 
**type:** (array) [] 

**required:** optional

**value:** 
```
allowedActionElement: [
    'cmb-option-prev', 
    'cmb-option-next', 
    'cmb-option-move', 
    'cmb-option-add', 
    'cmb-option-delete', 
    'cmb-option-copy', 
    'cmb-option-settings'
],
```
use when you want to show specific button. default button names are provided. just remove any button you do not want to you in the action element bar (move delete setting option bar)

##### avoidableTabs: 
**type:** string array or object

**required:** optional

**value:** 
```
[
    {
        general: ['tab1','tab2']
    },
    'display'
]
```
Setting options are multi dimentional and if any tabs are needed to be removed it needs to be in array or string finally. for instance if 1 tab needs to be removed, we can use string. if multi tab needs to be removed we can use array. if there is a condition where a tab1 is under general and we need to remove tab1 then we need to use an object and key will be `general` and value will be `tab1`. if there are tab1 and tab2 under general, we can use array. our common tabs are `general` `position` `design` `background`. listed item here will be avoided

##### avoidableTabSettings: 
**type:** loop of object and final item is array

**required:** optional

**value:** 
```
{
    position : ['align-self','float'],
    design : ['padding-left','padding-right']
},                     
```
always loop of tab as key name and finally array value for settings. listed item here will be avoided

available common options right now
```
{
    general : [
       'xs-hide','sm-hide','md-hide','lg-hide','xl-hide','class','id'
    ],
    position : [
        'float','display','flex','justify-content','align-content','align-item','align-self',
    ],
    design : [
        'padding-top','padding-right','padding-bottom','padding-left','margin-top','margin-right','margin-bottom','margin-left','text-align',
    ],
    background : [
        'background-color','background-image','background-position','background-attachment','background-repeat','background-size',
    ]
    
}
```

##### avoidableTabSettingOptions: 
**type:** loop of object and final item is array

**required:** optional

**value:** 
```
{
    position : {
        display : ['d-*-none','d-*-flex']
    }
}                   
```
always loop of tab or settings as key name and finally array value for options. in the example under `position` tab, `display` settings will avoid `d-*-none` and `d-*flex`

##### tabSettingOptionOnly: 
**type:** loop of object and final item is array

**required:** optional

**value:** 
```
{
    position : {
        display : ['d-*-none','d-*-flex']
    }
}                   
```
always loop of tab or settings as key name and finally array value for options. in the example under `position` tab, `display` settings will only have `d-*-none` and `d-*flex`

##### dynamicOptions: 
**type:** (object) {}

**required:** required if elementType == "dynamic-layout"

**value:** 
```
    dynamicOptions: {
        column: 3,
        item: 6,
        name: 'blog'
    }                  
```
must have key `name` and a unique name and other parameters for queryable data

##### settings: 
**type:** (object) {}

**required:** required but can be empty

**value:** 
```
settings:{
        tabname: {
            title: 'Column Setting',
            options: [
                {
                    settings1: {
                        title: 'Column',
                        // ....
                    },
                    settings2: {
                        title: 'Order',
                        // ....
                    },
                    offset: {
                        title: 'Offset',
                        // ....
                    },
                }
            ]
        }
    },              
```
settings must have a tabname as key and under value `title` and `options` are required. options will be array and inside that array an object. every single settings will go inside the final object.

######
|  name    |is required|    value    | details   |
|:--------:|:--------:|:------:|:------:|
|title|always required|responsive-class / input||
|type|always required|||
|optionName|required if inputType == "responsive-class"|cmbClassTypes key from class-setting.js||
|subTitle|required if inputType == "input"|||
|attribute|required if type == "input"|style / class / id / custom-class / prop / dynamic-layout||
|inputType|required if type == "input"|text / number / url / colorPicker / range / textarea / select / switch / dropdown / toggle / fourSided / fourCornered / image||
|defaultValue|required for toggle input type||mainly for toggle value, may be class name|
|styleName|required if type == "input"  and attribute == "style||value is css style attribute if single field , for 4 sided input first part (margin, padding),  wild card for border radius (border-*-radius), for border style first part only (border)|
|tagProp|if type=="input" and attribute == "prop"|name of the target prop||
|childItem|||if option is required for subitem dom|
|pseudo|if attribute == "style"||if pseudo is required for the item|
|childPseudo|if attribute == "style"||if pseudo is required for any child item|
|unit|optional||t is px,%,rem etc. if unit is available in param then param unit is used else if main object unit is available then that one is used|
|placeholder|optional||mainly usable for text , url , number , some of border where text fields are used , fourSided , borderRadius, image if placeholder is available in param then param placeholder is used else if main object placeholder is available then that one is used|
|wrapperClass|optional||it is added in the setting input wrapper if given|
|blackList|optional||used if attribute == ( class / id ) if any value is available in blackList, value is removed|
|fieldParam|Required for select, switch etc|`{}`| if fieldParam is available in param then param fieldParam is used else if main object fieldParam is available then that one is used|
|param||`{}`/`[]`|if single input, use 0 as key and options as object if multiple object (for styles) use style attribute subpart (i.e: for margin, use left/right) as key and options as object param can be array if only value is required|
|relatedDynamicField|if attribute=="dynamic-layout"||value is a key from dynamicOptions |


###### Param options

```
param options if object
//--------------
placeholder : '',       - optional, mainly usable for text , url , number , some of border where text fields are used , fourSided , borderRadius, image. if placeholder is available in param then param placeholder is used else if main object placeholder is available then that one is used

unit : [],              - optional. it is px,%,rem etc. if unit is available in param then param unit is used else if main object unit is available then that one is used

options : {},           - required for switch and select

fieldParam : {}         - Required for some input types (range). if fieldParam is available in param then param fieldParam is used else if main object fieldParam is available then that one is used
```

### Methods

#### element Methods

onLoad: function(){},

beforeAdd: function(wrapperItem, addableContent){},

afterAdd: function(addedItem){},

beforeClone: function(copyableItem){},

afterClone: function(addedItem){},

beforeMove: function(movableItem){},

afterMove: function(movedItem, previousParent){},

beforePrev: function(processableItem,futureIndex){},

afterPrev: function(processedItem,PreviousIndex){},

beforeNext: function(processableItem,futureIndex){},

afterNext: function(processedItem,PreviousIndex){},

beforeDelete: function(deletableItem){},

afterDelete: function(parent){},

beforeSave: function(){},

afterSave: function(){},


#### Setting methods (inside settings)

beforeChange(targetDom){}

afterChange(targetDom){}


### Follow These rules
do not use element name as id or class anywhere else

do not use prefix with `cmb_` anywhere but in element

do not use `cmb-element-tab-` prefix anywhere in class name 

do not use `cmb-settings-tab-` prefix anywhere in class name 

do not use `cmb-option-box-input` anywhere

### Forbidden Classes
cmb_*

cmb-element-tab-*

cmb-settings-tab-*

cmb-option-box-input



### Forbidden IDs
cmb_*

### Other Caution
setting tab name should not contain "|"