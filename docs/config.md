Config is the central point of your application configurations. Here you can change any of your application configurations from `config` directory. We added some additional configuration file for easier development.

`appsettings.php` holds all of your application settings fields and their attributes which is described in `App Control/Application Setting` section.

`commonconfig.php` holds all of your application specific configurations.

Some Configuration which must be taken in concern 

```

    *** All fixed roles must be placed in the array ***
    'fixed_roles' => [],    | i.e. USER_ROLE_ADMIN

    *** All fixed users must be placed in the array ***
    'fixed_users' => [],    | i.e. FIXED_USER_ADMIN

    *** If any field is needed to escape html tag, they must be listed here ***

    'strip_tags' => [

        *** The field name that will escape the tags included in allowed_tag_for_escape_text ***
        'escape_text' => [],

        *** The field name that will escape the tags included in allowed_tag_for_escape_full_text ***
        'escape_full_text' => [],

        *** Listed tags that will be passed by the inputs listed in escape_text ***
        'allowed_tag_for_escape_text' => '',

        *** Listed tags that will be passed by the inputs listed in escape_full_text ***
        'allowed_tag_for_escape_full_text' => '',
    ],
```

`navigation.php` holds menu registered places and menu template configurations which is described in `App Control/Navigation Setting` section.

`webpermission.php` holds all of your routes for `Role Management`  which is described in `App Control/Role Management` section.

`apipermission.php` holds all of your API routes for `Role Management` just like the `webpermission.php`
