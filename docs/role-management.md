The Role Managements where you can create a role and give the permissions. But you have to make sure that your route should be inside `routes/groups/permission.php` (for API `routes/groups/permission_api.php`) routes or your route must use `permission` middleware. (for API `permission_api`) You have to make sure another thing to give permission. Your route need to add inside `config/webpermission.php` (for API `config/apipermission.php`).

**Example:** You just create a resource route for your articles inside `routes/groups/permission.php`.

```
Route::resource('articles','ArticlesController');

'configurable_routes' => [
        'admin_section' => [
            'article' => [
                'reader_access' => [
                    'articles.index',
                    'articles.show'
                ],
            ],
        ],
    ],
```

Here under `configurable_routes` define section name > under section name define module name > under module name define route group that you want to display into permission list and under route group define routes according to your route group. This route group does not mean Laravel route group. you create your own group to display in role management page. 

##### Role Based Routes

If you want to define some routes that are only accessible by a specific predefine role then you have to define these roles to under `ROUTE_TYPE_ROLE_BASED` as follow: 

```
ROUTE_TYPE_ROLE_BASED => [
    USER_ROLE_ADMIN => [
        'application-settings.edit'
        'application-settings.update'
    ]
]
```

##### Avoidable Maintenance Routes

If you want to define some routes that are only accessible in maintenance mode then you have to define these roles to under `ROUTE_TYPE_AVOIDABLE_MAINTENANCE` as follow:  

```
 ROUTE_TYPE_AVOIDABLE_MAINTENANCE => [
    'login',
    'tickets.index',
]
```

##### Avoidable Unverified Routes 

If you want to define some routes that are only accessible when the user is not verified then you have to define these roles to under `ROUTE_TYPE_AVOIDABLE_UNVERIFIED` as follow:  

```
ROUTE_TYPE_AVOIDABLE_UNVERIFIED => [
    'logout',
    'profile.index',
    'notifications.index',
    'notifications.mark-as-read'
]
```

##### Avoidable Inactive Routes 

If you want to define some routes that are only accessible when the user is not active then you have to define these roles to under `ROUTE_TYPE_AVOIDABLE_INACTIVE` as follow:  

```
ROUTE_TYPE_AVOIDABLE_INACTIVE => [
    'logout',
    'profile.index',
    'notifications.index',
    'notifications.mark-as-read',
    'notifications.mark-as-unread',
]
```

##### FInancial Routes 

If you want to define some routes that are doing some financial process then you have to define these roles to under `ROUTE_TYPE_FINANCIAL` as follow: 

```
ROUTE_TYPE_FINANCIAL => [
    'deposits.create'
    'deposits.store'
]
```

##### Globally Common Routes 

If you want to define some routes that are accessible by all users then you have to define these roles to under `ROUTE_TYPE_GLOBAL` as follow: 

```
ROUTE_TYPE_GLOBAL => [
    'logout',
    'profile.index',
    'profile.edit',
    'profile.update',
    'profile.change-password',
    'profile.update-password',
    'profile.avatar.edit',
    'profile.avatar.update',
    'notifications.index',
    'notifications.mark-as-read',
    'notifications.mark-as-unread',
]
```

**Note: After finishing the configuration manage your permission from role managements. Otherwise you cannot access the routes.**

**Note: Superadmin user can access all routes without permissions. A system will have only one superidmin user**

![Web Role Management Page](./images/role-management.png)



