#### Default Layout: Path: 
`resources/views/layouts/`

`master.blade.php` 
##### Calling a parameter with the layout
```
@extends('layouts.master',[parameter=>value])
```
|Field Type| Values | Default | Description |
|:--------:|:------:|:------:|:-----------:|
|headerLess|true / false|false|If true, the page loads layout without header, footer, breadcrumb and sidebar|
|hideBreadcrumb|true / false|false|If true, the page loads no breadcrumb|
|transparentHeader|true / false|false|If true, the page loads transparentHeader ignoring the application setting
|inversedLogo|true / false|false|If true, the page loads invered color of actual logo ignoring the application setting|

#### Layout Parts:
path: `resources/views/layouts/includes`
##### breadcrumb.blade.php
It contains breadcrumb for a page
##### footer.blade.php
It contains footer section of a page
##### header.blade.php
It contains `html` and `head` tag and all `css` inside the `head` tag with the starting of `body` tag and `div` with id `app` for vue js.
##### list-css.blade.php
This contains bootstrap date time picker and datatable css. It is required when we use datatable and search box in a page for listing data.
##### list-js.blade.php
It contains moment.js, bootstrap date time picker and datatable js. It is required when we use datatable and search box in a page for listing data.
##### script.blade.php
It contains side nav inside it with message box and default scripts
##### side_nav.blade.php
It contains the layout of side nav
##### top_header.blade.php
It contains total top header including top nav, log and user section.

