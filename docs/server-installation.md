# **Installation Process**

## Server Requirement

* Linux Server (Preferred Ubuntu 20.04)
* RAM 2GB
* Space 40GB
* Apache 2.4/Nginx Server (Preferred Nginx)
* PHP 7.4+ 
* Database (Preferred MYSQL 5.7+)
* PHP Dependency Manager (Preferred Composer)

**Note: The above server configuration is minimum requirement.**  


## Prerequisites PHP extension

* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Curl PHP extension
* GD PHP extension
* GMP PHP extension
* BCMath PHP Extension
* ZIP PHP extension
* MYSQL PHP extension


## Server Installation For Linux 

Login to your server using ssh from your terminal. After successfully logged in follow every instructions down bellow.

**Note: You can install either apache ar nginx. Here we describe the nginx installation.**

#### Installing Nginx Server

Nginx is available in Ubuntu’s default repositories, it is possible to install it from these repositories using the apt packaging system.

Since this is our first interaction with the apt packaging system in this session, we will update our local package index so that we have access to the most recent package listings. Afterwards, we can install nginx:

```
sudo apt update 
sudo apt install -y nginx 
```
When you have your server’s IP address, enter it into your browser’s address bar: 
You should see the default Nginx landing page

```
http://your_server_ip
```

To additional configuration, you need to go through the full installation process. [Full Nginx Installation Guide](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04){:target="_blank"}

#### Installing PHP/PHP-FPM

Unlike Apache, Nginx doesn’t have a built in support for processing PHP files so we need to install a separate application such as PHP FPM (“fastCGI process manager”) which will handle PHP files.

To install the PHP and PHP FPM packages run the following command:

```
sudo apt install -y php7.4-fpm
```

#### Installing PHP Extension

To run the system you need install some additional extensions. PHP extensions are available as packages and can be easily installed with. 

To install the required php extensions run the following command:

```
sudo apt install -y php7.4-{mbstring,mysql,gd,gmp,xml,curl,json,zip,bcmath}

```

#### Installing MYSQL

To install MYSQL go through every step describe in the link. [MYSQL Installation Guide](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04){:target="_blank"}

#### Installing Composer 

To install composer (PHP dependency manager) run the following command:
```
sudo apt install -y composer
```

Or you can follow the link: [Composer Installation Guide](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-20-04){:target="_blank"}


#### Installing NodeJS and NPM (Optional)

To install a different version of Node.js, you can use a PPA (personal package archive) maintained by NodeSource. 

First, we will install the PPA in order to get access to its packages. From your home directory, use curl to retrieve the installation script for your preferred version, making sure to replace 12.x with your preferred version string (if different).

```
curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt install nodejs
```

## Server Installation For Windows 

#### Installing Laragon

To install PHP, Nginx/Apache, MYSQL, and NodeJS at once you can install Laragon. It is a portable, isolated, fast & powerful universal development environment for PHP, Node.js. It is fast, lightweight, easy-to-use and easy-to-extend.

Follow the link to install Laragon: [Laragon Installation Guide](https://laragon.org/docs/install.html){:target="_blank"}


