#### Blade components
Path: `resources/view/components`

We have currenctly 4 components

1. Alert
2. Card
3. Info Box
4. Table

**N.B.: All types of slot is optional. When it needed place it anywhere indide the component. ** 


##### Alert
To use alert components in blade  
```
    @component('components.alert',['type' => 'primary', 'class' => 'mb-4 rounded-0'])
        <h3 class="card-title">Title goes here....</h3>
    @endcomponent
```


###### Attributes

|Name|Value|Description|
|:----|:---------|:------|
|type|primary,info,success,danger,warning,secondary,light,dark|Alert type|
|class|any class name without `alert`|To provide additional class|
|id|unique id name|To provide id into alert|
|closeButton|true,false|To hide and show close button|


##### Card
To use card components in blade  
```
@component('components.card',['type' => 'primary'])
    @slot('header')
        <h3>Header goes here........</h3>
    @endslot

    <p>Body goes here............</p>

    @slot('footer')
        <p>Footer goes here........</p>
    @endslot
@endcomponent
```


###### Attributes

|Name|Value|Description|
|:----|:---------|:------|
|type|primary,info,success,danger,warning,secondary,light,dark|Card type|
|class|any class name without `card`|To provide additional class|
|id|unique id name|To provide id into card|
|headerClass|any valid class name|To provide any class into card header div|
|bodyClass|any valid class name|To provide any class into card body div|
|bodyClass|any valid class name|To provide any class into card body div|
|footerClass|any valid class name|To provide any class into card footer div|


##### Info Box
To use info box components in blade  
```
    @component('components.info_box',['type' => 'primary'])
        <p>Content goes here...........</p>
    @endcomponent
```


###### Attributes

|Name|Value|Description|
|:----|:---------|:------|
|type|primary,info,success,danger,warning,secondary,light,dark|Info box type|
|class|any class name without `info-box`|To provide additional class|
|id|unique id name|To provide id into info box|
|icon|any icon element|To change icon into info box|

##### Table
To use info box components in blade  
```
    @component('components.table',['class' => 'lf-data-table'])
        @slot('thead')
            <tr>
                <th>Table header goes here...........</th>
            </tr>
        @endslot

        <tr>
            <td>Table body goes here...........</td>
        </tr>

        @slot('tfoot')
            <p>Table footer goes here...........</p>
        @endslot
    @endcomponent
```


###### Attributes

|Name|Value|Description|
|:----|:---------|:------|
|type|primary,info,success,danger,warning,secondary,light,dark|Table type|
|class|any class name without `table`|To provide additional class|
|id|unique id name|To provide id into table|